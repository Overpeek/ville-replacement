import { NavLink } from 'react-router-dom'
import Submenu from './components/Submenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import style from './AppHeader.css'
import style2 from './components/Header.css'

function AppHeader(props) {
    return (
        <header className={style.AppHeader}>
          <div className={style2.header_left}>
            <span className={style.logo}>Aava</span>
            <nav>
              <NavLink to="/">Etusivu</NavLink>
            </nav>
            <nav>
              <a href="/swagger-ui/#/">SwaggerUI</a>
            </nav>
          </div>

          <div className={style2.header_right}>
            <Submenu className={style.submenu}
                     items={
                         [
                             { content: "Asetukset" },
                             { content: "Kirjaudu ulos" }
                         ]
                     }
                     buttonContent={
                         [
                             <span>{props.user?.name}</span>,
                             <span className={style.bars}><FontAwesomeIcon icon={faBars}/></span>
                         ]
                     }
            />
          </div>
        </header>
    )
}

export default AppHeader;
