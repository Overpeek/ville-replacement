import React from 'react';
import LinkBar from './components/LinkBar';
import FrontpageBox from './components/FrontpageBox'
import Calendar from 'react-calendar'
import '!style-loader!css-loader!react-calendar/dist/Calendar.css';

class FrontPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            courses: props.courses
        }
    }

    componentDidMount() {
        
    }

    courseLink = ({name, id}) => (
        <LinkBar
          key={id}
          to={"/courses/" + id}
          leftSide={
              <span className="course-name">
                {name}
              </span>
          }
        />
    )

    render() {
        return (
            <div>
              <FrontpageBox
                title="Kurssit"
                content={
                  this.state.courses.map(this.courseLink)
                }
              />
              <FrontpageBox
                title="Uutiset"
                content={
                  this.state.courses.map(this.courseLink)
                }
              />
              <FrontpageBox
                className="calendar"
                title="Kalenteri"
                aboveContent={
                    <Calendar
                      defaultView="month"
                      minDetail="year"
                      onClickDay={undefined}
                    />
                }
                content={
                  this.state.courses.map(this.courseLink)
                }
              />
            </div>
        )
    }

}

export default FrontPage
