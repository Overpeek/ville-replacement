import React from 'react';
import LinkBar from './components/LinkBar'
import { useParams } from 'react-router-dom'
import CourseHeader from './components/CourseHeader'
import style from './CoursePage.css'

function withParams(Component) {
    return props => <Component {...props} params={useParams()} />;
}

class CoursePage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            course: this.props.getCourse(this.props.params.id),
        }
    }

    componentDidMount() {
        
    }

    lessonLink = ({name, id, score, maxScore}) => (
        <LinkBar
          key={id}
          to={"lessons/" + id}
          leftSide={
              <span className={style.lesson_name}>
                {name}
              </span>
          }
          rightSide={
              <span className={style.lesson_score}>
                {score}/{maxScore}
              </span>
          }
        />
    )
    lessons = () => ([
        {
            name: "1. Johdanto",
            id: 1,
            score: 5,
            maxScore: 10,
            content: [
                {
                    type: "text",
                    content: "jskdfj ksdlj kdsjf klsdj fkldsj fklsd jfklds jlkfj skldjfklsjdflkdsjk fjsdkl jfkldsj fkldsj kldjf klsdj fkldjs fkls jdfk ljdskl jklds jkldjsfk jdsklf jkldsj fklsd jfklds jfklsdj kdslj kdsj fklsdj fkljsf kljds kljsd kljds kl jfdsklj fklsdjf kldsj fkldjsf k"
                },
                {   type: "exercise",
                    title: "1. Eka tehtävä",
                    score: 5,
                    maxScore: 10,
                }
            ]
        },
        {
            name: "2. Asd asd ads",
            id: 2,
            score: 5,
            maxScore: 10,
            content: [
                {
                    type: "text",
                    content: "jskdfj ksdlj kdsjf klsdj fkldsj fklsd jfklds jlkfj skldjfklsjdflkdsjk fjsdkl jfkldsj fkldsj kldjf klsdj fkldjs fkls jdfk ljdskl jklds jkldjsfk jdsklf jkldsj fklsd jfklds jfklsdj kdslj kdsj fklsdj fkljsf kljds kljsd kljds kl jfdsklj fklsdjf kldsj fkldjsf k"
                },
                {   type: "exercise",
                    title: "1. Eka tehtävä",
                    score: 5,
                    maxScore: 10,
                }
            ]
        },
    ])



    render() {
        return (
            <div>
              <CourseHeader
                course={{name:this.state.course.name, id:this.state.course.id}}
                rightSide={
                    <button className="btn">Tietoa</button>
                }
              />
              <div className={style.course_container}>
                <div className={style.course_progress}>
                  <meter value="40" max="100">
                  </meter>
                </div>
                <div className="course-content">
                  {this.lessons().map(this.lessonLink)}
                </div>
              </div>
            </div>
        )
    }

}

export default withParams(CoursePage)
