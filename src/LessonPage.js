import React from 'react';
import PythonExercise from './components/PythonExercise';
import CourseHeader from './components/CourseHeader';

import style1 from './LessonPage.css'
import style2 from './CoursePage.css'

let style = {
    ...style1,
    ...style2
};

class LessonPage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            course: props.course,
            lesson: props.lesson,
        }
    }

    componentDidMount() {
        
    }

    render() {
        return (
            <div>
              <CourseHeader
                course={{
                    name: this.state.course.name,
                    id: this.state.course.id
                }}
                lessonName={
                    this.state.lesson.name
                }
                rightSide={
                    <button className="btn">
                      Tietoa
                    </button>
                }
              />

              <div className={style.lesson_container}>
                <div className={style.lesson_progress}>
                  <meter value="40" max="100">
                  </meter>
                </div>
                <h1 className={style.lesson_title}>{this.state.lesson.name}</h1>
                <div className={style.lesson_content}>
                  <div className={style.lesson_text}>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Velit ut tortor pretium viverra suspendisse potenti nullam ac. Sit amet porttitor eget dolor. Lacinia at quis risus sed vulputate odio ut. Erat imperdiet sed euismod nisi. Orci sagittis eu volutpat odio facilisis mauris. Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. Elit eget gravida cum sociis natoque penatibus et. At elementum eu facilisis sed odio morbi quis commodo. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula. Tincidunt dui ut ornare lectus sit amet est placerat. Feugiat scelerisque varius morbi enim nunc faucibus. Neque ornare aenean euismod elementum nisi. Sed risus ultricies tristique nulla aliquet. Amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Eu augue ut lectus arcu bibendum. Arcu ac tortor dignissim convallis aenean et tortor at risus. Egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris.</p>
                    <pre>
                      <code>
                        {
`a = 0
b = int(input("Anna luku: "))
if b > a:
print("Luku oli positiivinen")`
                        }
                      </code>
                    </pre>
                    <p>Ut ornare lectus sit amet. Consequat mauris nunc congue nisi vitae suscipit tellus mauris. Mauris pharetra et ultrices neque ornare aenean euismod. Quisque sagittis purus sit amet volutpat consequat mauris nunc congue. Viverra nibh cras pulvinar mattis nunc. Urna porttitor rhoncus dolor purus non enim. Posuere morbi leo urna molestie at elementum eu. Augue eget arcu dictum varius duis at consectetur. Pharetra massa massa ultricies mi. Donec ac odio tempor orci dapibus ultrices. Eu ultrices vitae auctor eu augue ut. Ut tortor pretium viverra suspendisse potenti nullam ac tortor vitae. Nulla facilisi etiam dignissim diam quis enim lobortis scelerisque. Risus viverra adipiscing at in tellus integer feugiat scelerisque varius. Id diam maecenas ultricies mi eget mauris.</p>
                    <p>Imperdiet sed euismod nisi porta lorem mollis aliquam ut porttitor. Nullam vehicula ipsum a arcu cursus vitae congue. Sed blandit libero volutpat sed cras ornare. Dignissim cras tincidunt lobortis feugiat vivamus at augue eget. Et ligula ullamcorper malesuada proin libero nunc consequat. Feugiat nisl pretium fusce id velit ut tortor pretium. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at. Dui faucibus in ornare quam viverra orci sagittis. Vulputate dignissim suspendisse in est. Et magnis dis parturient montes. Lorem sed risus ultricies tristique nulla aliquet enim tortor. Senectus et netus et malesuada fames ac. Tortor at auctor urna nunc. Est ante in nibh mauris cursus mattis. Cras tincidunt lobortis feugiat vivamus at. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Lacus vestibulum sed arcu non odio euismod lacinia.</p>
                    <p>Lorem ipsum dolor sit amet. Lectus magna fringilla urna porttitor rhoncus dolor purus non. Purus in mollis nunc sed id. Volutpat lacus laoreet non curabitur gravida arcu ac. At erat pellentesque adipiscing commodo elit at imperdiet. Elementum tempus egestas sed sed risus. Quis viverra nibh cras pulvinar. Iaculis nunc sed augue lacus. Odio morbi quis commodo odio. Velit ut tortor pretium viverra suspendisse potenti. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh. In mollis nunc sed id semper risus in hendrerit. Diam quis enim lobortis scelerisque fermentum. Viverra aliquet eget sit amet tellus cras adipiscing. Aliquet enim tortor at auctor urna nunc id cursus. Viverra accumsan in nisl nisi scelerisque eu. Vestibulum lorem sed risus ultricies tristique nulla aliquet enim tortor. Risus feugiat in ante metus dictum. Ultricies lacus sed turpis tincidunt id aliquet risus. Velit egestas dui id ornare arcu odio ut.</p>
                  </div>
                  <PythonExercise
                    exercise={{
                        name: "1. Eka tehtävä",
                        description: [
                            <p>Tallenna muuttujaan jokin arvo, joka ei ole merkkijono (esimerkiksi numero).</p>,
                            <p>Tulosta tämän muuttujan arvo osana jotakin merkkijonoa.</p>
                        ],
                        id: 1,
                        score: 5,
                        maxScore: 10,
                    }}
                  />
                </div>
              </div>
            </div>
        )
    }
}

export default LessonPage
