import React from 'react';
import {
    Route,
    Routes,
    BrowserRouter,
} from "react-router-dom";

import FrontPage from "./FrontPage";
import LessonPage from "./LessonPage";
import CoursePage from "./CoursePage";
import AppHeader from "./AppHeader";

class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            
        }
    }

    componentDidMount() {
        this.setState({
            user: {
                name: "Olli Opiskelija",
                studentNumber: 2378273,
                courses: [
                    1,2,3
                ]
            }
        })
    }

    courses = () => {
        return([
            {
                name: "Kurssi 4578",
                id: 1,
                description: "niau niau niau niau niau niau",
                lessons: [
                    {
                        id: 1,
                        score: 50,
                        maxScore: 100,
                    },
                    {
                        id: 2,
                        score: 20,
                        maxScore: 140,
                    },
                ]
            },
            {
                name: "Ohjelmoinnin perusteet",
                id: 2,
                description: "niau niau niau niau niau niau",
                lessons: [
                    {
                        id: 3,
                        score: 50,
                        maxScore: 100,
                    },
                    {
                        id: 4,
                        score: 20,
                        maxScore: 140,
                    },
                ]
            },

        ])
    }

    getCourse = (id) => {
        return(this.courses().filter(course => course.id == id)[0])
    }

    lessons = () => ([
        {
            name: "1. Johdanto",
            id: 1,
            content: [
                {
                    type: "text",
                    content: "jskdfj ksdlj kdsjf klsdj fkldsj fklsd jfklds jlkfj skldjfklsjdflkdsjk fjsdkl jfkldsj fkldsj kldjf klsdj fkldjs fkls jdfk ljdskl jklds jkldjsfk jdsklf jkldsj fklsd jfklds jfklsdj kdslj kdsj fklsdj fkljsf kljds kljsd kljds kl jfdsklj fklsdjf kldsj fkldjsf k"
                },
                {   type: "exercise",
                    title: "1. Eka tehtävä",
                    score: 5,
                    maxScore: 10,
                }
            ]
        },
        {
            name: "2. Asd asd ads",
            id: 2,
            content: [
                {
                    type: "text",
                    content: "jskdfj ksdlj kdsjf klsdj fkldsj fklsd jfklds jlkfj skldjfklsjdflkdsjk fjsdkl jfkldsj fkldsj kldjf klsdj fkldjs fkls jdfk ljdskl jklds jkldjsfk jdsklf jkldsj fklsd jfklds jfklsdj kdslj kdsj fklsdj fkljsf kljds kljsd kljds kl jfdsklj fklsdjf kldsj fkldjsf k"
                },
                {   type: "exercise",
                    title: "1. Eka tehtävä",
                    score: 5,
                    maxScore: 10,
                }
            ]
        },
    ])
    
    frontPage = () => ({
        url: "/",
        page: <FrontPage 
                courses={this.courses()}
              />
    })

    coursePage = () => ({
        page: <CoursePage
                getCourse={this.getCourse}
                course={this.courses()[0]}
              />
    })

    esimerkki = () => ({
        page: <LessonPage 
                course={this.courses()[0]}
                lesson={this.lessons()[0]}
              />
    })

    render() {
        return (
            <div className="App">
              <BrowserRouter>
                <AppHeader user={this.state.user}/>
                <Routes>
                  <Route
                    path="/"
                    element={this.frontPage().page}/>
                  <Route
                    path="/courses/:id"
                    element={this.coursePage().page}/>
                  <Route
                    path="/courses/:courseId/lessons/:id"
                    element={this.esimerkki().page} />
                </Routes>
              </BrowserRouter>
            </div>
        );
    }
}

export default App;
