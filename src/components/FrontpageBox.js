import Header from './Header'
import style from './FrontpageBox.css'

function FrontpageBox(props) {
    return (
        <div className={style.frontpage_box + " " + props.className}>
          <Header
            className={style.header}
            leftSide={
                <span>{props.title}</span>
            }/>
          <div className={style.nonscrollable}>
            {props.aboveContent}
          </div>
          <div className={style.scrollable}>
            {props.content}
          </div>
        </div>
    )
}

export default FrontpageBox
