import AceEditor from "react-ace";
import { useState } from "react";
import Header from './Header'
import ExerciseFrame from './ExerciseFrame'
import PopupWindow from './PopupWindow'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons'

import "ace-builds/src-noconflict/mode-python";
import "ace-builds/src-noconflict/theme-xcode";
import "ace-builds/src-noconflict/ext-language_tools";

import style from './PythonExercise.css'

import * as pyvm from 'pyvm';

function PythonExercise(props) {
    const [showPopup, setShowPopup] = useState(false);

    const [content, setContent] = useState("for i in range(0, 10):\n\tprint(i)");

    const [stdout, setStdout] = useState("");

    const [fontSize, setFontSize] = useState(16);
    const increaseFontSize = () => {
        let newFontSize = Math.min(fontSize+1, 25)
        setFontSize(newFontSize)
    }
    const decreaseFontSize = () => {
        let newFontSize = Math.max(fontSize-1, 10)
        setFontSize(newFontSize)
    }

    let onChange = (newContent) => {
        setContent(newContent)
    }

    const runPython = () => {
        pyvm.main({ code: content, stdin: "" }).then(async resp => {
            console.log(resp);
            setStdout(resp.stdout);
            setShowPopup(true);
        });
    }

    const submitCode = () => {
      fetch("/api/exercise/c52de49c-5dba-4820-bf64-0da20f9f339f/submit", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                code: content,
            })
      }).then(async _resp => {
            let resp = _resp.json();
            console.log(await resp.json());
            setStdout(resp.stdout);
            setShowPopup(true);
        })
    }

    let buttons = () => (
        [
            <button className="btn">
              Reset
            </button>,
            <button className="btn" onClick={runPython}>
              Test
            </button>,
            <button className="btn" onClick={submitCode}>
              Submit
            </button>
        ]
    )

    return (
        <ExerciseFrame exercise={props.exercise}>
          {
              showPopup && (<PopupWindow content={stdout} closeFunction={() => setShowPopup(false)}/>)
          }
          <div className={style.content}>
            <div className={style.coding_area_wrapper}>
              <Header
                className={style.coding_area_header}
                leftSide={
                    <span>Koodieditori</span>
                }
                rightSide={
                    <div className={style.fontsize_btns}>
                      <button
                        className={style.fontsize_btn}
                        onClick={decreaseFontSize}
                      >
                        <FontAwesomeIcon icon={faMinus} />
                      </button>
                      <button
                        className={style.fontsize_btn}
                        onClick={increaseFontSize}
                      >
                        <FontAwesomeIcon icon={faPlus} />
                      </button>
                    </div>

                }
              />
              <div className={style.coding_area}>
                <AceEditor
                  placeholder=""
                  mode="python"
                  theme="xcode"
                  name={"python-exercise-"+props.exercise.id+"-code-editor"}
                  width=""
                  onLoad={undefined}
                  onChange={onChange}
                  fontSize={fontSize}
                  showPrintMargin={false}
                  showGutter={true}
                  highlightActiveLine={true}
                  value={content}
                  editorProps={{ $blockScrolling: true }}
                  setOptions={{
                      enableBasicAutocompletion: true,
                      enableLiveAutocompletion: false,
                      enableSnippets: false,
                      showLineNumbers: true,
                      tabSize: 4,
                  }}
                />
              </div>
            </div>
            <div className={style.buttons}>{buttons()}</div>
            <div className={style.description}>
              {props.exercise.description}
            </div>
          </div>
        </ExerciseFrame>
    )
}

export default PythonExercise
