import style from './Header.css'

function Header(props) {
  return(
    <header 
      className={style.Header + " " + props.className}
      onClick={props.onClick}
    >
      <div className={style.header_left}>
        {props.leftSide}
      </div>
      <div className={style.header_right}>
        {props.rightSide}
      </div>
    </header>
  )
}

export default Header
