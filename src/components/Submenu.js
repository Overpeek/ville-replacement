import { useState, useEffect, useRef } from 'react'
import style from './Submenu.css'

function Submenu(props) {
    const [ display, setDisplay ] = useState( 'none' )

    function handleClick() {
        if ( display === 'none' ) {
            setDisplay( 'block' )
        } else {
            setDisplay( 'none' )
        }
    }

    function useOutsideAlerter(ref) {
      useEffect(() => {
        function handleClickOutside(event) {
          if (ref.current && !ref.current.contains(event.target)) {
            setDisplay( 'none' )
          }
        }
        // Bind the event listener
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
          // Unbind the event listener on clean up
          document.removeEventListener("mousedown", handleClickOutside);
        };
      }, [ref]);
    }


    const menuItem = (item) => {
        return(
            <div
              key={item.content}
              className={"item " + style.item}>
              <span>{item.content}</span>
            </div>
        )
    }
    
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    const className = props.className ? " " + props.className : ""

    return (
        <div
          className={"submenu " + style.submenu + " " + className}
          onClick={handleClick}
          ref={wrapperRef}>
          <div
            onClick={handleClick}
            className={"button " + style.button}>
            {props.buttonContent}
          </div>
          <div
            className={"dropdown " + style.dropdown}
            style={{display:display}}>
            {props.items.map(menuItem)}
          </div>
        </div>
    )
}

export default Submenu
