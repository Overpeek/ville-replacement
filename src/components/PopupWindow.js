import Header from './Header'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark } from '@fortawesome/free-solid-svg-icons'

import style from './PopupWindow.css'

function PopupWindow(props) {
    return (
        <div className={style.popup_window}>
          <Header
            leftSide={<span>{props.title}</span>}
            rightSide={
                <button 
                  className={style.close_btn}
                  onClick={props.closeFunction}
                >
                  <FontAwesomeIcon icon={faXmark} />
                </button>
            }
          />
          <div className={style.content}>
            {props.content}
          </div>
        </div>
    )
}

export default PopupWindow
