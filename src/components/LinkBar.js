import { Link } from 'react-router-dom'

import style from './LinkBar.css'

function LinkBar(props) {
    return (
        <Link
          className={"link_bar " + style.link_bar}
          to={props.to}>
          <div className={style.link_bar_left}>
            {props.leftSide}
          </div>
          <div className={style.link_bar_right}>
            {props.rightSide}
          </div>
        </Link>
    )
}

export default LinkBar
