import Header from './Header'
import { Link } from 'react-router-dom'

function CourseHeader(props) {
    const name = props.course.name;
    const id = props.course.id;
    const lesson = props.lessonName ? props.lessonName : "";
    return (
        <Header
          leftSide={[
            <Link
              key={id+"-course-link"}
              to={"/courses/" + id}>
              {name}
            </Link>,
            <span
              key={id+"-lesson-name"}>
              {lesson}
            </span>
          ]}
          rightSide={props.rightSide}
        />
    )
}

export default CourseHeader
