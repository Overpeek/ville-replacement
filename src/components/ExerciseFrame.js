import { useState, useRef, useEffect } from "react";
import Header from './Header';

import style from './ExerciseFrame.css'

function ExerciseFrame(props) {

    const ref = useRef(null);

    const [open, setOpen] = useState(false);
    const toggleOpen = () => {
        const newValue = !open
        setOpen(newValue)
    }
    useEffect(() => {
        if (open) {
            ref.current?.scrollIntoView({behavior: 'smooth'});
        }
    }, [open]);

    /*
    const [showingDescription, setShowingDescription] = useState("");
    const toggleDescription = () => {
        setShowingDescription(!showingDescription)
    }*/

    const header = () => (
        <Header
          className={style.header}
          onClick={toggleOpen}
          leftSide={
              <span>
                {props.exercise.name}
              </span>
          }
          rightSide={
              <span>
                {props.exercise.score}/{props.exercise.maxScore}
              </span>
          }
        />
    )

    if (!open) {
        return(header())
    }

    /* {showingDescription ? <PopupWindow
               title="Description" 
               content={props.description}
               closeFunction={() => {setShowingDescription(false)}}
           /> : ""}
    */

    return (
        <div
          className={style.exercise}
          ref={ref}>
          {header()}
          {props.children}
        </div>
    )
}

export default ExerciseFrame
